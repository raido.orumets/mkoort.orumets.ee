function OfferCalculator(form, table) {

    {
        this.form = form;
        this.table = table;
        this.tempTableRow = null;
    }

    this.loadTempTableRow = function () {
        this.tempTableRow = $('tbody tr:first', this.table).remove().clone(true);
        this.resetNumbering();
    }

    this.appendTableRow = function () {
        let row = $(this.tempTableRow).clone(true).appendTo('tbody', this.table);
        this.resetNumbering();
        this.triggerEvent('offer_calculator_table_row_added', {
            row: row,
        });
    }

    this.triggerEvent = function (name, variables) {
        const event = new CustomEvent(name, variables);
        document.dispatchEvent(event);
    }

    this.addTableRow = function () {
        this.appendTableRow();
    }

    this.removeTableRow = function (row) {
        $(row, table).remove();
        this.resetNumbering();
    }

    this.resetNumbering = function () {
        let count = $('tbody tr', this.table).length;
        let nr = 0;
        $('tbody tr', this.table).each(function () {
            nr++;
            $('.nr', this).text(nr);
        });
    }

    this.getData = function () {
        // https://jquerypluginsfree.com/serialize-an-html-form-to-a-javascript-object-supporting-nested-attributes-and-arrays/
        return $(this.form).serializeJSON();
    }

    this.submit = function (callbackFunction) {
        let obj = this;
        $(this.form).submit(function (event) {
            let data = obj.getData();

            console.log('Submit OfferCalculator', data);

            let submitForm = callbackFunction(event, obj.form, data);
            return (submitForm == true) ? true : false;
        });
    }

    this.init = function () {
        console.info('Init OfferCalculator');

        let obj = this;

        obj.loadTempTableRow();

        $('.btn-add-row', obj.form).click(function () {
            obj.addTableRow();
        });

        document.addEventListener("offer_calculator_table_row_added", function (event) {
            $('.btn-remove-row', obj.table).click(function () {
                console.log('btn-remove-row');
                let rowElement = $(this).closest('tr');
                obj.removeTableRow(rowElement);
            });
        });
    }
}